#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 8 15:24:51 2020
@author: Tobias Bonifay
"""
from ipv4_str import chaine_vers_ipv4
from check_ipv4 import check_ipv4
from math import log2, ceil


def initial_ipv4_address():
    """
    Request the initial IP address from the user as long as it is not in the form of a string such as 192.68.24.0.
    ================
    :return: a valid IP address or None for exiting
    :rtype: str or None for exiting
    """
    cl_ipv4 = None
    print("type\'q\' to exit")
    while cl_ipv4 is None:
        ipv4 = input("Input an IP: ")
        l_ipv4 = chaine_vers_ipv4(ipv4)  # str to list
        cl_ipv4 = check_ipv4(l_ipv4)  # check the Ipv4 list, return None or IP
        if ipv4 == "q":
            return None
    return cl_ipv4


def initial_mask():
    """
    Ask the user for an integer, check that it is an integer between 0 and 32.
    ================
    :return: mask between 0 and 32
    :rtype: int
    """
    mask = -1
    while not 0 <= mask <= 32:
        try:
            mask = int(input("Input mask: "))
        except ValueError:
            print("You must input a VALID mask")
            print("You need an integer between 0 and 32")
    return mask


def subnetwork_number():
    """
    Ask the user for a positive integer
    How many subnetworks should we create with the initial IP ?
    ================
    :return: number of subnetwork
    :rtype: int
    """
    nsr = int()
    while not nsr > 0:
        try:
            nsr = int(input("Input the number of subnetwork: "))
        except ValueError:
            print("You must input a number of subnetwork")
            print("You may type an integer above 0")
    return nsr


def hosts_subnet(nsr, mask):
    """
    For each of the subnets, ask the user for an integer corresponding to the number of hosts in that subnet, verify.
    ================
    :param nsr: number of subnets
    :type nsr: int
    :param mask: CIDR
    :type mask: int
    :return: list the number of hosts for each subnet
    :rtype: list
    """
    l0_host = list()   # number of hosts asked
    nb_ask = int()  # number of hosts needed by the user

    for i in range(1, nsr + 1):
        val = None
        while type(val) != int:
            try:
                val = int(input("How many host for subnetwork " + str(i) + ": "))
                if val <= 0:
                    raise ValueError
                l0_host.append(val)
                nb_ask += val
            except ValueError:
                print("Number of host is invalid for this subnet: ", i)

    available_hosts = int(2 ** (int(32 - mask)) - 2)
    if available_hosts >= nb_ask:
        return l0_host
    else:
        print("It's impossible")
        return None


def calcul_prefixe_sous_reseau(l_hotes):
    """
    Calculate the subnet mask with the number of hosts
    ================
    :param l_hotes: number of hosts for each subnet
    :type l_hotes: list
    :return: subnet mask
    """
    l_mask = list()
    for elt in l_hotes:
        l_mask.append(32 - ceil(log2(elt)))
    return l_mask


def int_to_ip(int_ip):
    """
    Convert ip from integer to list using f strings
    ================
    :param int_ip: ip
    :type int_ip: int
    :return: ip
    :rtype: list
    """
    ip = list()
    bin_ip = f"{int_ip:032b}"
    for i in range(0, 32, 8):
        bin_elem = bin_ip[i:i + 8]
        ip.append(int(bin_elem, 2))
    return ip


def ip_to_int(ip):
    """
    Convert ip from list to int using f strings
    ================
    :param ip: ip
    :type ip: list
    :return: ip
    :rtype: int
    """
    string_ip = str()
    for elem in ip:
        string_ip += f"{elem:08b}"
    return int(string_ip, 2)


def calcul_plan_adressage(l_add_init, l_hotes, l_mask):
    """
    Calculate first and last address of each subnet with broadcast address
    ================
    :param l_add_init: first ip address
    :type l_add_init: list
    :param l_hotes: list of hosts
    :type l_hotes: list
    :param l_mask: list of masks
    :type l_mask: list
    :return: printing
    :rtype: None
    """
    for i, a in enumerate(l_hotes):

        print(f"SR {i}    {l_add_init} / {l_mask[i]}")
        print(f"1@      {int_to_ip(ip_to_int(l_add_init) + 1)}")

        ip_amount = 2 ** ceil(log2(a))
        ip_next_reseau = list()
        for elem in l_add_init[::-1]:
            ip_amount, r = divmod(ip_amount, 256)
            ip_next_reseau.insert(0, elem + r)
        l_add_init = ip_next_reseau

        int_next_ip = ip_to_int(l_add_init)
        print(f"L@      {int_to_ip(int_next_ip - 2)}")
        print(f"bcast   {int_to_ip(int_next_ip - 1)}")
        print("")


def main():
    #  The initial network address in the form of a list of 4 integers (each integer representing a byte)
    l_add_init = initial_ipv4_address()
    if l_add_init is None:
        return None

    # The initial network mask in prefix format as integer
    masque_init = initial_mask()

    # The number of subnetworks desired as an integer
    nsr = subnetwork_number()

    # The number of hosts per subnetwork as a list of integers
    l_hotes = hosts_subnet(nsr, masque_init)
    if l_hotes is None:
        return None

    # Ordering subnets
    l_hotes.sort(reverse=True)

    # Subnet Mask Calculator
    #  The list of subnet masks in prefixed notation
    l_mask = calcul_prefixe_sous_reseau(l_hotes)

    # Subnet Ip Calculator
    # The list of network addresses (each network address is itself represented by a list, so it will be a list of list)
    calcul_plan_adressage(l_add_init, l_hotes, l_mask)


if __name__ == "__main__":
    main()
